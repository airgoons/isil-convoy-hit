# Syria Convoy Hit

This is a B-Mission that aspires to be an A-mission.  It seats 4 F/A-18s and 4 F-14s.

The flight is tasked with taking down an ISIL convoy smuggling goods within Syria.  Pro-government forces will take offense to our presence and try to shoot us down.

Flight includes multiple optional objectives that reduce SAM and air threat, and multiple approaches to objective completion.

## Feedback

- Hot start good for this mission, that convoy was more dangerous than expected.
- Aleppo could be made to be an unlockable spawn once protecting SA2 dies.
- On scale up - potential great mission for A-10s, maybe add some firefights around Aleppo for them to CAS.
- F/A-18s or JF-17s using GMTI could acquire targets and buddy lase for A-10s, which would be a new and unique experience for goons.
- Stealth approach worked beautifully, as did airfield disable.
- Possible to fork as-is for a low tech mission.
- HARM group on scale-up