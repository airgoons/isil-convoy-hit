READYREFRESHTIME=1800 --Time in seconds before airframe inventories refresh.
RefreshTimer = nil

sqn679=SQUADRON:New("SYRIA-MIG21", 1, "679 Squadron MiG21")
sqn679:SetSkill(AI.Skill.AVERAGE)
sqn679:AddMissionCapability({AUFTRAG.Type.INTERCEPT, AUFTRAG.Type.CAP , AUFTRAG.Type.ESCORT}, 50)
sqn679.reservegroups=3
sqn679.maxreadygroups=1
sqn679.readyrefreshincrement=1
sqn678=SQUADRON:New("SYRIA-MIG23", 1, "678 Squadron MiG23")
sqn678:SetSkill(AI.Skill.AVERAGE)
sqn678:AddMissionCapability({AUFTRAG.Type.INTERCEPT, AUFTRAG.Type.CAP , AUFTRAG.Type.ESCORT}, 60)
sqn679.reservegroups=3
sqn679.maxreadygroups=1
sqn679.readyrefreshincrement=1

abuAlDuhurAirwing=AIRWING:New("Abu al-Duhur Warehouse", "678 Squadron")
hamaAirwing=AIRWING:New("Hama Warehouse", "679 Squadron")

abuAlDuhurAirwing:AddSquadron(sqn678)
hamaAirwing:AddSquadron(sqn679)

abuAlDuhurAirwing:NewPayload(GROUP:FindByName("SYRIA-MIG23-A2A"), -1 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT}, 50)
hamaAirwing:NewPayload(GROUP:FindByName("SYRIA-MIG21-A2A"), -1 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT}, 50)

abuAlDuhurAirwing:SetMarker(false)
abuAlDuhurAirwing:SetReportOff()
abuAlDuhurAirwing:Start()
hamaAirwing:SetMarker(false)
hamaAirwing:SetReportOff()
hamaAirwing:Start()

local AgentSet=SET_GROUP:New():FilterCoalitions("red"):FilterStart()

syriaBorderZone = ZONE_POLYGON:New( "Syria", GROUP:FindByName( "syriaBorderZone" ) )
local syriaIntel=INTEL:New(AgentSet, coalition.side.red) 
syriaIntel:AddAcceptZone(syriaBorderZone) 
syriaIntel:Start()

function refreshInventory() 
    env.info("Refresh Called")
    if sqn679:CountAssetsInStock()  < sqn679.maxreadygroups and sqn679.reservegroups >= sqn679.readyrefreshincrement then
      hamaAirwing:AddAssetToSquadron(sqn679,sqn679.readyrefreshincrement)
    end
    if sqn678:CountAssetsInStock()  < sqn678.maxreadygroups and sqn678.reservegroups >= sqn678.readyrefreshincrement then
      abuAlDuhurAirwing:AddAssetToSquadron(sqn678,sqn678.readyrefreshincrement)
    end
end

function syriaIntel:OnAfterNewContact(From, Event, To, Contact)
  local contact=Contact
  ATO(contact, {abuAlDuhurAirwing,hamaAirwing})
end

function ATO(contact, airwings)
  local auftrag 
  if contact.attribute == "Air_Bomber" then
    local grp= GROUP:FindByName(contact.groupname)
    local vec2 = grp:GetVec2()
    local coord = grp:GetCoordinate()
    local zoneR = ZONE_RADIUS:New("CapZone", vec2, 90000)
    auftrag=AUFTRAG:NewCAP(zoneR, 25000, 300, coord, 2700, 10)
    auftrag:SetRepeatOnFailure(5)
  elseif contact.attribute == "Air_Fighter" then
    local grp= GROUP:FindByName(contact.groupname)
    auftrag=AUFTRAG:NewINTERCEPT(grp)
    auftrag:SetRepeatOnFailure(5)
    auftrag:SetPriority(70, true)
  end
  
  if auftrag ~= nil then
    for _, airwing in pairs(airwings) do
      airwing:AddMission(auftrag)
    end
    if RefreshTimer == nil then
      RefreshTimer = TIMER:New(refreshInventory)
      RefreshTimer:Start(READYREFRESHTIME,READYREFRESHTIME)
    end
  end
end