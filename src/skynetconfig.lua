syriaIADS = SkynetIADS:create('Syria')
syriaIADS:addSAMSitesByPrefix('SYRIA-SAM')
syriaIADS:addEarlyWarningRadarsByPrefix('SYRIA-EWR')

syriaIADS:getSAMSitesByPrefix('SYRIA-SAM-SA2'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):setGoLiveRangeInPercent(104)
syriaIADS:getSAMSitesByPrefix('SYRIA-SAM-SA3'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):setGoLiveRangeInPercent(104)
syriaIADS:getSAMSitesByPrefix('SYRIA-SAM-SA6'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):setGoLiveRangeInPercent(105)
syriaIADS:getSAMSitesByPrefix('SYRIA-SAM-SA8'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):setGoLiveRangeInPercent(106)
syriaIADS:getSAMSitesByPrefix('SYRIA-SAM-SA10'):setActAsEW(true)
syriaIADS:getSAMSitesByPrefix('SYRIA-SAM-SA15'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):setGoLiveRangeInPercent(105)

local bu59 = syriaIADS:getSAMSiteByGroupName('SYRIA-SAM-SA15-BU59')
syriaIADS:getSAMSiteByGroupName('SYRIA-SAM-SA10-BU49'):addPointDefence(bu59):setIgnoreHARMSWhilePointDefencesHaveAmmo(true)
local bt41 = syriaIADS:getSAMSiteByGroupName('SYRIA-SAM-SA8-BT41')
syriaIADS:getSAMSiteByGroupName('SYRIA-SAM-SA3-BT41'):addPointDefence(bt41):setIgnoreHARMSWhilePointDefencesHaveAmmo(true)
syriaIADS:getEarlyWarningRadarByUnitName('SYRIA-EWR-55G6-BT41'):addPointDefence(bt41):setIgnoreHARMSWhilePointDefencesHaveAmmo(true)
local bt92 = syriaIADS:getSAMSiteByGroupName('SYRIA-SAM-SA8-BT92')
syriaIADS:getSAMSiteByGroupName('SYRIA-SAM-SA2-BT92'):addPointDefence(bt92):setIgnoreHARMSWhilePointDefencesHaveAmmo(true)
local bu88 = syriaIADS:getSAMSiteByGroupName('SYRIA-SAM-SA8-BU88')
syriaIADS:getSAMSiteByGroupName('SYRIA-SAM-SA6-BU88'):addPointDefence(bu88):setIgnoreHARMSWhilePointDefencesHaveAmmo(true)

local syriaDebug = syriaIADS:getDebugSettings()
--in game
syriaDebug.IADSStatus = false
syriaDebug.contacts = false
syriaDebug.jammerProbability = false
--dcs.log
syriaDebug.addedEWRadar = false
syriaDebug.addedSAMSite = false
syriaDebug.warnings = false
syriaDebug.radarWentLive = false
syriaDebug.radarWentDark = false
syriaDebug.harmDefence = false
--detailed logging
syriaDebug.samSiteStatusEnvOutput = false
syriaDebug.earlyWarningRadarStatusEnvOutput = false
syriaDebug.commandCenterStatusEnvOutput = false

syriaIADS:setupSAMSitesAndThenActivate()

